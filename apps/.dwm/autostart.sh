sxhkd &
nitrogen --restore &
dunst &
picom --config ~/.picom.conf &
/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

#xfce4-power-manager &
setxkbmap us,lt -option 'grp:alt_shift_toggle' &

dwmblocks &

exec dwm
