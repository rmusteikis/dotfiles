source ~/.config/nvim/plugins.vim

let os = system("uname | awk '$1'")
if os =~ 'Linux'
  let g:python_host_prog = '/usr/bin/python'
  let g:python3_host_prog = '/usr/bin/python3'
elseif os =~ 'Darwin'
  let g:python_host_prog = '/usr/local/bin/python'
  let g:python3_host_prog = '/usr/local/bin/python3'
endif

" global
let g:mapleader="\<Space>"

set mouse=a

set number relativenumber

set hidden
set nobackup
set nowritebackup
set cmdheight=2
set updatetime=300
set shortmess+=c
set splitbelow
set splitright
set shiftwidth=2
set smartcase
set ignorecase
set incsearch
set linebreak
set textwidth=120
set showmatch
set hlsearch
set autoindent
set expandtab
set shiftwidth=2
set shiftround
set softtabstop=2
set list listchars=tab:⟶\ ,trail:·,extends:>,precedes:<,nbsp:%
set showtabline=1
set lazyredraw

"" tabs navigation
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <C-Up> :tabnew <right>
nnoremap <C-Down> :tabclose<CR>

"" colors
set termguicolors
"let ayucolor="light"  " for light version of theme
"let ayucolor="mirage" " for mirage version of theme
"let ayucolor="dark"   " for dark version of theme
colorscheme base16-circus
"colorscheme dracula
"colorscheme one
"set background=dark

"" misc
" hitting escape will also clean highlighting
noremap <silent> <Esc> <Esc>:noh<cr>
" i hit Q by mistake.
nnoremap Q <nop>

inoremap ;esf <C-R>iexport const NAME = () => {<Enter>return <div></div> <Enter>}<Esc>kk/NAME<Enter>ciw
inoremap ;ir <C-R>iimport React from 'react';<Enter>
inoremap ;ef <C-R>iexport function NAME() {<Enter>return <div></div><Enter>}<Esc>kkk/NAME<Enter>ciw
inoremap ;fc <C-R>ifunction NAME() {<Enter>return <div></div><Enter>}<Esc>kkk/NAME<Enter>ciw
inoremap ;caf <C-R>iconst NAME = () => {<Enter><Enter>}<Esc>kk/NAME<Enter>ciw
inoremap ;af <C-R>i() => 

