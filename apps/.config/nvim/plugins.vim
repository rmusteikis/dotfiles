call plug#begin('~/.vim/plugged')

" completion, etc.
Plug 'neoclide/coc.nvim', {'branch': 'release'}
autocmd CursorHold * silent call CocActionAsync('highlight')
command! -nargs=0 Prettier :CocCommand prettier.formatFile

Plug 'mattn/emmet-vim'
let g:user_emmet_leader_key=','

" file management
Plug 'preservim/nerdtree'
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
nnoremap <C-p> :Files<CR> 
nnoremap <C-b> :Buffers<CR> 

Plug 'tpope/vim-fugitive'
autocmd BufReadPost fugitive://* set bufhidden=delete

" style
Plug 'rakr/vim-one'
Plug 'flazz/vim-colorschemes'
Plug 'ayu-theme/ayu-vim'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'chriskempson/base16-vim'
Plug 'ryanoasis/vim-devicons'
Plug 'vim-airline/vim-airline'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 0

Plug 'vim-airline/vim-airline-themes'

" syntax
Plug 'preservim/nerdcommenter'
map <C-n> :NERDTreeToggle<CR>

Plug 'jiangmiao/auto-pairs'
let g:AutoPairsCenterLine = 0
let g:AutoPairsMultilineClose = 0
let g:AutoPairsFlyMode = 1

"Plug 'tpope/vim-surround'
"Plug 'tpope/vim-repeat'

"Plug 'posva/vim-vue'
"let g:vue_pre_processors = ['scss']

Plug 'mxw/vim-jsx'
let g:jsx_ext_required = 0

Plug 'pangloss/vim-javascript'
Plug 'leafgarland/typescript-vim'
Plug 'moll/vim-node'
Plug 'cakebaker/scss-syntax.vim', { 'for': 'css' }
"Plug 'nikvdp/ejs-syntax'
"Plug 'plasticboy/vim-markdown'
"Plug 'mustache/vim-mustache-handlebars'

call plug#end()
