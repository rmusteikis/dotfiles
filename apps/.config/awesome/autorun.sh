#!/usr/bin/env bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run compton --config=$HOME/.compton.conf
run nitrogen --restore
