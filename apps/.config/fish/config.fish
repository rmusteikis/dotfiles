#!/bin/fish
#set -xU LESS_TERMCAP_md (printf "\e[01;31m")
#set -xU LESS_TERMCAP_me (printf "\e[0m")
#set -xU LESS_TERMCAP_se (printf "\e[0m")
#set -xU LESS_TERMCAP_so (printf "\e[01;44;33m")
#set -xU LESS_TERMCAP_ue (printf "\e[0m")
#set -xU LESS_TERMCAP_us (printf "\e[01;32m")

set EDITOR nvim

alias xrdb='xrdb -load ~/.Xresources'
alias v='nvim'
alias vi='nvim'
alias vim='nvim'
alias gall='git add . && git commit -m "updated" && git push'
alias pdf='zathura ~/Documents/master-2020/masterRM-T.pdf'
alias master='cd ~/Documents/master-2020/'

# Base16 Shell
if status --is-interactive
    set BASE16_SHELL "$HOME/.config/base16-shell/"
    source "$BASE16_SHELL/profile_helper.fish"
end
