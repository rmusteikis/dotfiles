#!/bin/sh
batCapacity=$(cat /sys/class/power_supply/BAT0/capacity)
batStatus=$(cat /sys/class/power_supply/BAT0/status)

batStatusSymbol=" "

bold=$(tput bold)
norm=$(tput sgr0)

if [ $batStatus == "Charging" ]
then
   batStatusSymbol="+"
elif [ $batStatus = "Discharging" ]
then
   batStatusSymbol="-"
else
   batStatusSymbol="*"
fi

printf " %s$bold%s$norm\\n" "$batCapacity" "$batStatusSymbol"

