#!/bin/sh
vol=$(amixer get Master | tail -n1 | grep -Po '\[\K[^%]*' | head -n1)
mute="$(amixer -c 0 get Master | tail -1 | awk '{print $6}' | sed 's/[^a-z]*//g')"

if [[ $volume == 0 || "$mute" == "off" ]]
then
	printf "[muted] \\n"
else
	printf " %s \\n" "$vol"
fi
