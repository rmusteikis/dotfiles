//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"", "~/.config/dwmblocks/spotify.py",		1,		0},
	{"", "~/.config/dwmblocks/wifi.sh",				5,		0}, /* wifi */
	{"", "~/.config/dwmblocks/layouts.sh",		1,		0},
	{"", "~/.config/dwmblocks/vol.sh",				0,		12},
	{"", "~/.config/dwmblocks/light.sh",			0,		11},
	{"", "~/.config/dwmblocks/bat.sh",				60,		0},
	{"", "~/.config/dwmblocks/time.sh",				60,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
