#!/bin/bash

MENU="dmenu -i -p "-Exit?""
C=$(echo -e "YES\nNO\n" | $MENU)

case "$C" in
  YES) bspc quit ;;
  NO) exit 0;;
esac
