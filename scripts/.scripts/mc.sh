#!/bin/sh 

case "$1" in
  toggle) amixer sset Master toggle;;
  mute) amixer sset Master mute;;
  up) amixer sset Master 5%+;;
  down) amixer sset Master 5%-;;
esac >/dev/null
