#!/bin/sh
dte(){
  dte="$(date +'%b %d, %H:%M')"
  echo -e "$dte"
}

upd(){
  upd=`checkupdates | wc -l`
  echo -e "$upd upd."
}

while true; do
  xsetroot -name "$(dte)"
  sleep 1m
done
