#!/bin/zsh

if [ $(pgrep emacs 2> /dev/null | wc -l ) -eq 0 ]; then
    emacs --daemon
    dunstify "Emacs Daemon" "Launched: emacs --daemon"
    exit 0
elif [ $(pgrep emacs 2> /dev/null | wc -l ) -ne 0 ]; then
        dunstify "Emacs Daemon" "Already launched!"
        exit 0
fi
