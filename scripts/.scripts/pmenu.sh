#!/bin/bash


MENU="dmenu -i -p "-PowerMenu""
C=$(echo -e " Lock\n Logout\n Reboot\n Shutdown\n" | $MENU)
            case "$C" in
                *Lock) i3lock-fancy -t "" ;;
                *Logout) ~/.scripts/exit.sh ;;
                *Reboot) ~/.scripts/reboot.sh  ;;
                *Shutdown) ~/.scripts/poweroff.sh
              esac
