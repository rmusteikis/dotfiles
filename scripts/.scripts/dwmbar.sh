#!/bin/zsh

## This script sets the statusbar with the xsetroot command at the end. Have it
## started by ~/.xinitrc or ~/.xprofile.
#
## Handle SIGTRAP signals sent by refbar to update the status bar immediately.
#trap 'update' 5
#
## Set the deliminter character.
#delim=" "
#
## Here is the (big) function that outputs the appearance of the statusbar. It
## can really be broken down into many submodules which I've commented and
## explained.
#status() { \
#
#	# Date and time.
#  date '+%a %H:%M'
#  echo "$delim"
#
#	# Get the volume of ALSA's master volume output.  Show an icon if or
#	# not muted.
#	amixer get Master | grep -o "[0-9]*%\|\[on\]\|\[off\]" | sed "s/\[on\]//;s/\[off\]//" | tail -n 1
#
#	echo "$delim"
#
#	# Will show all batteries with approximate icon for remaining power.
#  if [ "$(cat /sys/class/power_supply/AC0/online)" -eq "0" ];
#  then
#    for x in /sys/class/power_supply/BAT?/capacity;
#    do
#      RED='\033[00;31m'
#      case "$(cat $x)" in
#        100|9[0-9])	echo "" ;;
#        8[0-9]|7[0-9])	echo "" ;;
#        6[0-9]|5[0-9])	echo "" ;;
#        4[0-9]|3[0-9])	echo "" ;;
#        *)		echo "" ;;
#      esac
#    done 
#  else 
#    echo " $(cat /sys/class/power_supply/BAT?/capacity)%"
#  fi
#  
#  #echo "$delim"
#
#	}
#
#update() { \
#	# So all that big status function was just a command that quicking gets
#	# what we want to be the statusbar. This xsetroot command is what sets
#	# it. Note that the tr command replaces newlines with spaces. This is
#	# to prevent some weird issues that cause significant slowing of
#	# everything in dwm. Note entirely sure of the cause, but again, the tr
#	# command easily avoids it.
#	xsetroot -name "$(status | tr '\n' ' ')" &
#    wait
#
#	# Check to see if new weather report is needed.
#	#testweather &
#  #  wait
#  }


while :; do
#    update
	# Sleep for a minute after changing the status bar before updating it
	# again. We run sleep in the background and use wait until it finishes,
    # because traps can interrupt wait immediately, but they can't do that
    # with sleep.
    (conky | while read LINE; do xsetroot -name "$LINE"; done) &
    sleep 1s &
    wait
done
