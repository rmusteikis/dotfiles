#!/bin/bash
# changeVolume

# Arbitrary but unique message id
msgId="991949"
 
# Change the volume using alsa(might differ if you use pulseaudio)
#amixer -c 0 set Master "$@" > /dev/null

# Query amixer for the current volume and whether or not the speaker is muted
light="$( light | rev | cut -c4- | rev )"
if [[ $light == 0 ]]; then
    # Show the sound muted notification
    dunstify -r "$msgId" "DARKNESS" 
else
    # Show the volume notification
    dunstify -r "$msgId" "Light: ${light}%"
fi

