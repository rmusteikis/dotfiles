#exports
export VISUAL="nvim"
export EDITOR="nvim"
export MANPAGER="nvim +Man!"

source ~/.dotfiles/zshrc/.antigen/antigen.zsh

# plugins
antigen use oh-my-zsh

antigen bundle git
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle taskwarrior

antigen theme af-magic

antigen apply

#aliases
alias v="nvim"

# task
alias tl="task ls"
alias tin="task add +in"
alias ta="task active"

# git
alias g="git"
alias ga="git add"
alias gc="git commit -m"
alias gp="git push"
alias gpl="git pull"
alias gs="git status"
alias gd="git diff"
alias ga="git add"

alias tree="find . -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'"

alias maxpower="sudo cpupower frequency-set -g performance"
alias minpower="sudo cpupower frequency-set -g powersave"

#config aliases
alias vconf="v ~/.config/nvim/init.vim"

## combo git alias func
gall() {
	git add $1 && git commit -m $2 && git push
}

unsetopt BEEP

